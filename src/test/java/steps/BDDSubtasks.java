package steps;

import static org.junit.Assert.fail;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.HomePage;
import pages.LoginPage;
import pages.MyTasksPage;
import pages.SubtaskPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BDDSubtasks {
	private WebDriver driver;

	String assertionError = null, subtasksCompare = null, dateCompare = null ;

	LoginPage loginPage = new LoginPage(driver);
	MyTasksPage myTasksPage = new MyTasksPage(driver);
	HomePage homePage = new HomePage(driver);
	SubtaskPage subtaskPage = new SubtaskPage(driver);

	@Given("estou na tela {string} acesso o link informo email de usuario {string} e senha {string}")
	public void estouNaTelaAcessoOLinkInformoEmailDeUsuarioESenha(String site, String user,	String password) {
		try {

			// Abrir o navegador
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\paulo\\OneDrive\\Alura\\Drive\\chromedriver.exe");
			driver = new ChromeDriver();

			// Acessar a aplica��o
			driver.get(site);

			// Acesso a p�gina de login
			loginPage.clickAccessPageLogin();

			// Informar usu�rio e senha, dentro da fun��o ele executa logar
			loginPage.login(user, password);

		} catch (Exception e) {
			assertionError = e.toString();
			fail(assertionError);
		}
	}

	@When("aciono o comando de Manage Subtrack {string} que esta criado")
	public void acionoOComandoDeManageSubtrackQueEstaCriado(String newSubtasks) {
		try {
			myTasksPage.myNewTask(newSubtasks);
		} catch (Exception e) {
			assertionError = e.toString();
			fail(assertionError);
		}
	}

	@When("preencho os campos de descricao {string} e a data {string} para a tasks {string}")
	public void preenchoOsCamposDeDescricaoEADataParaATasks(String nameSubtask, String date,
			String nameTask) {
		try {
			subtaskPage.manageSubtasks(nameTask);
			subtaskPage.newNameSubtask(nameSubtask);
			subtaskPage.dateSubtask(date);
			subtasksCompare = nameSubtask;
			dateCompare = date;
		} catch (Exception e) {
			assertionError = e.toString();
			fail(assertionError);
		}
	}

	@When("aciono comando adicionar")
	public void acionoComandoAdicionar() {
		try {
			subtaskPage.addNewSubtasks();
			verificaQuantidadeCaracteres(subtasksCompare);
			verificaData(dateCompare);
		} catch (Exception e) {
			assertionError = e.toString();
			fail(assertionError);
		}
	}

	@Then("uma nova Subtask criada")
	public void umaNovaSubtaskCriada() {
		try {
			driver.close();
		} catch (Exception e) {
			assertionError = e.toString();
			fail(assertionError);
		}
	}
	
	public void verificaQuantidadeCaracteres(String mensagem) {
		if (mensagem.length() == 0) {
			driver.close();
			fail("Campo obrigatorio preenchimento");
		} else if (mensagem.length() > 250) {
			driver.close();
			fail("Deve ter no m�ximo 250 caracteres");
		} else if (mensagem.length() < 3) {
			driver.close();
			fail("Deve ter no m�nimo 3 caracteres");
		}
	}
	
	public void verificaData(String mensagem) {
		String s = mensagem;
		try {
			DateFormat testeData = new SimpleDateFormat("MM/dd/yyyy");
			testeData.setLenient(false);
			testeData.parse(s);
		} catch (Exception e) {
			assertionError = e.toString();
			driver.close();
			fail(assertionError);
		}
	}

}

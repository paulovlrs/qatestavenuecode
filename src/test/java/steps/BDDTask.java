package steps;

//import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.HomePage;
import pages.LoginPage;
import pages.MyTasksPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BDDTask {

	private WebDriver driver;

	String assertionError = null, compare = null;

	LoginPage loginPage = new LoginPage(driver);
	MyTasksPage myTasksPage = new MyTasksPage(driver);
	HomePage homePage = new HomePage(driver);

	@Given("acesso a tela {string} acesso o link informo email de usuario {string} e senha {string}")
	public void acessoATelaAcessoOLinkInformoEmailDeUsuarioESenha(String site, String user, String password) {
		try {

			// Abrir o navegador
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\paulo\\OneDrive\\Alura\\Drive\\chromedriver.exe");
			driver = new ChromeDriver();

			// Acessar a aplica��o
			driver.get(site);

			// Acesso a p�gina de login
			loginPage.clickAccessPageLogin();

			// Informar usu�rio e senha, dentro da fun��o ele executa logar
			loginPage.login(user, password);

		} catch (Exception e) {
			assertionError = e.toString();
			fail(assertionError);
		}
	}

	@Given("acionei o comando My Tasks")
	public void acioneiOComandoMyTasks() {
		try {
			// Aciono o bot�o My Tasks
			homePage.clickButtonMyTask();
		} catch (Exception e) {
			assertionError = e.toString();
			fail(assertionError);
		}
	}

	@When("preencho o campo de descricao da nova task {string}")
	public void preenchoOCampoDeDescricaoDaNovaTask(String nameTask) {
		try {// Informo o nome da nova task e aciono o adicionar
			myTasksPage.setTaskName(nameTask);
			compare = nameTask;
		} catch (Exception e) {
			assertionError = e.toString();
			fail(assertionError);
		}
	}

	@When("aciono comando adicionar usando as teclas do teclado")
	public void acionoComandoAdicionarUsandoAsTeclasDoTeclado() {
		try {
			myTasksPage.pressEnterTaskName();
			verificaQuantidadeCaracteres(compare);
		} catch (Exception e) {
			assertionError = e.toString();
			fail(assertionError);
		}
	}

	@Then("uma nova task criada")
	public void umaNovaTaskCriada() {
		try {
			driver.close();
		} catch (Exception e) {
			assertionError = e.toString();
			fail(assertionError);
		}
	}

	@When("aciono comando adicionar com o mouse")
	public void acionoComandoAdicionarComOMouse() {
		try {
			myTasksPage.addMyTasksButton();
			verificaQuantidadeCaracteres(compare);
		} catch (Exception e) {
			assertionError = e.toString();
			fail(assertionError);
		}
	}
	
	public void verificaQuantidadeCaracteres(String mensagem) {
		if (mensagem.length() > 250) {
			driver.close();
			fail("Deve ter no m�ximo 250 caracteres");
		} else if (mensagem.length() < 3) {
			driver.close();
			fail("Deve ter no m�nimo 3 caracteres");
		}
	}
}

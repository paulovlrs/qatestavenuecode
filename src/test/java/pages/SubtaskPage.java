package pages;

import org.openqa.selenium.By;
//import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SubtaskPage {
	//private static final Object Element = null;

	WebDriver driver;

	/*@FindBy(how = How.XPATH, using = "//h3[starts-with ( text () , 'Task')]")
	WebElement idTask;*/

	@FindBy(how = How.NAME, using = "edit_task")
	WebElement editNameTask;

	@FindBy(how = How.NAME, using = "new_sub_task")
	WebElement descriptionNameSubtask;

	@FindBy(how = How.ID, using = "dueDate")
	WebElement inforDateSubtask;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='close()']")
	WebElement clickButtonCloseSubtask;

	@FindBy(how = How.ID, using = "add-subtask")
	WebElement clickButtonAddSubtask;

	public void createSubtask(String nameTask, String date) throws InterruptedException {
		newNameSubtask(nameTask);
		dateSubtask(date);
		// Foi necess�rio colocar o tempo de pausa, pois ao clicar em salvar n�o reconhecia o preenchimento
		//driver.manage().timeouts().pageLoadTimeout(3, TimeUnit.SECONDS);
		Thread.sleep(2000);
		addNewSubtasks();
		closeSubtask();
	}
/*
	public String checkIdTask() {
		//WebElement element = driver.findElements(By.cssSelector("//h3[@class='modal-title ng-binding']/text()")).get(0);
		 //String innerhtml = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML;", Element);
		//return innerhtml;
		return idTask.getText();
	}*/

	public void manageSubtasks(String task) { // Seleciono a task que desejo editar baseado no nome
		driver.findElement(By.xpath("//a[text()='" + task + "']/../..//button[contains(text(), 'Manage')]")).click();
	}

	public void addNewSubtasks() {
		clickButtonAddSubtask.click();
	}

	public void closeSubtask() {
		clickButtonCloseSubtask.click();
	}

	public void dateSubtask(String date) {
		inforDateSubtask.clear();
		inforDateSubtask.sendKeys(date);
	}

	public void newNameSubtask(String newSubtask) {
		descriptionNameSubtask.sendKeys(newSubtask);
	}

	public void editNameTask(String newName) {
		editNameTask.clear();
		editNameTask.sendKeys(newName);
	}

	public SubtaskPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
}

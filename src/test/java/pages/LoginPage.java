package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	WebDriver driver;

	@FindBy(how = How.ID, using = "user_email")
	WebElement userName;

	@FindBy(how = How.ID, using = "user_password")
	WebElement userPassword;

	@FindBy(how = How.NAME, using = "commit")
	WebElement clickButtonLogin;

	@FindBy(how = How.XPATH, using = "//*[@href='/users/sign_in']")
	WebElement changePageLogin;

	// Actions
	public void login(String userName, String userPassword) {
		setUserName(userName);
		setPassword(userPassword);
		clickLogin();
	}

	public void setUserName(String user) {
		userName.sendKeys(user);
	}

	public void setPassword(String password) {
		userPassword.sendKeys(password);
	}

	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickLogin() {
		clickButtonLogin.click();
	}

	public void clickAccessPageLogin() {
		changePageLogin.click();
	}
}

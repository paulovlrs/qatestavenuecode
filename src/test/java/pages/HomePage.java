package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	WebDriver driver;

	@FindBy(how = How.XPATH, using = "//div[@class='alert alert-info']")
	WebElement messageLoginSuccess;

	@FindBy(how = How.XPATH, using = "//a[@href='/tasks']")
	WebElement changePageLinkMyTasks;

	@FindBy(how = How.LINK_TEXT, using = "Home")
	WebElement changePageHome;
	
	@FindBy(how = How.XPATH, using = "//a[@class='btn btn-lg btn-success']")
	WebElement changePageButtonMyTasks;

	public String loginSucess() {
		return messageLoginSuccess.getText();
	}

	// redireciona para a p�gina de My Tasks atrav�s do link
	public void changeMyTasks() {
		changePageLinkMyTasks.click();
	}

	// redireciona para a p�gina principal
	public void changeHome() {
		changePageHome.click();
	}

	// redireciona para a p�gina de My Tasks atrav�s do button
	public void clickButtonMyTask() {
		changePageButtonMyTasks.click();
	}

	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
}

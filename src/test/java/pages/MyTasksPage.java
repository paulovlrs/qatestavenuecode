package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class MyTasksPage {
	
	WebDriver driver;

	@FindBy(how = How.XPATH, using = "//h1")
	WebElement titleNameToDo;

	@FindBy(how = How.ID, using = "new_task")
	WebElement taskDescription;

	@FindBy(how = How.XPATH, using = "//span[@ng-click='addTask()']")
	WebElement addTaskButton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn btn-primary']")
	WebElement confirmEditButtonTask;
	
	@FindBy(how = How.XPATH, using = "//button[class='btn btn-default']")
	WebElement cancelEditButtonTask;
	
	// Simplifico duas a��es informar nome da task e adicionar
	public void myNewTask(String newTask) {
		setTaskName(newTask);
		addMyTasksButton();
	}

	// Seleciono a task pelo o nome informado
	public void deleteTaskButton(String delete) {	// Foi necess�rio sair do padr�o para tratar desse cen�rio especifico
		// Localiza a task pelo o nome e a aciona o bot�o remover
		driver.findElement(By.xpath("//a[text()="+delete+"']/../..//button[contains(text(), 'Remove')]")).click();
	}

	// Verifico a mensagem que informa no sistema
	public String titleName() {
		return titleNameToDo.getText(); // N�o usar, est� dando pau por enquanto
	}
	
	// Informo o nome da task no campo
		public void setTaskName(String newTask) {
			taskDescription.sendKeys(newTask);
		}
		
		// Informo o nome da task no campo
		public void pressEnterTaskName() {
			taskDescription.sendKeys(Keys.RETURN);
		}
	
	// Aciono o bot�o de cancelar as modifica��es feitas
	public void cancelEditNameTask() {
		cancelEditButtonTask.click();
	}
	
	// Seleciono a task que desejo editar, limpo, informo o novo nome e confirmo
	public void editTaskName(String oldTasks, String newTask) {
		driver.findElement(By.xpath("//a[text()="+oldTasks+"']")).click();
		driver.findElement(By.xpath("//a[text()="+oldTasks+"']")).clear();
		taskDescription.sendKeys(newTask);
		confirmEditButtonTask.click();
	}

	// Aciono o bot�o de adicionar nova task
	public void addMyTasksButton() {
		addTaskButton.click();
	}

	public MyTasksPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
}

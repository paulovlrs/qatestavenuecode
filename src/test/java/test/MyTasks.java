package test;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.HomePage;
import pages.LoginPage;
import pages.MyTasksPage;

public class MyTasks {

	String assertionError = null;

	@Test
	public void myTasksTest() {
		try {
			// Abrir o navegador
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\paulo\\OneDrive\\Alura\\Drive\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			
			// Acessar a aplica��o
			driver.get("https://qa-test.avenuecode.com/");

			LoginPage loginPage = new LoginPage(driver);

			// Acesso a p�gina de login
			loginPage.clickAccessPageLogin();

			// Informar usu�rio e senha, dentro da fun��o ele executa logar
			loginPage.login("paulovlrs@hotmail.com.br", "123456789");

			HomePage homePage = new HomePage(driver);
			// Aciono o bot�o My Tasks
			homePage.clickButtonMyTask();
			
			MyTasksPage myTasksPage = new MyTasksPage(driver);
			
			// Informo o nome da nova task e aciono o adicionar
			myTasksPage.myNewTask("NovaTask");
			
			driver.close();

		} catch (Exception e) {
			assertionError = e.toString();
			fail(assertionError);
		}
	}
}

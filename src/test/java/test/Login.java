package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.HomePage;
import pages.LoginPage;

public class Login {

	String assertionError = null;

	@Test
	public void loginTest() {
		try {
			// Abrir o navegador
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\paulo\\OneDrive\\Alura\\Drive\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			// Acessar a aplica��o
			driver.get("https://qa-test.avenuecode.com/");

			LoginPage loginPage = new LoginPage(driver);

			// Acesso a p�gina de login
			loginPage.clickAccessPageLogin();

			// Informar usu�rio e senha, dentro da fun��o ele executa logar
			loginPage.login("paulovlrs@outlook.com", "123456789");

			HomePage homePage = new HomePage(driver);

			String x = homePage.loginSucess();
			//verifico se exibe mensagem de login com sucesso
			assertEquals("Signed in successfully.", x);

			// fechar navegador
			driver.close();
		} catch (Exception e) {
			assertionError = e.toString();
			fail(assertionError);
		}
	}
}
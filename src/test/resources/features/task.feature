Feature: Como um usuario do App ToDo eu deveria ser capaz de criar uma tarefa para que eu possa gerenciar minhas tarefas
 
 Background: 
    Given acesso a tela "https://qa-test.avenuecode.com" acesso o link informo email de usuario "paulovlrs@hotmail.com.br" e senha "123456789"
    And acionei o comando My Tasks
 
 Scenario: Criar uma Task salvando com Enter
 		When preencho o campo de descricao da nova task "prova1"
 		And aciono comando adicionar usando as teclas do teclado
 		Then uma nova task criada
 		
 Scenario: Criar uma Task salvando usando o mouse
 		When preencho o campo de descricao da nova task "prova2"
 		And aciono comando adicionar com o mouse
 		Then uma nova task criada
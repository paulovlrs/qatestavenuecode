$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/task.feature");
formatter.feature({
  "name": "Como um usuario do App ToDo eu deveria ser capaz de criar uma tarefa para que eu possa gerenciar minhas tarefas",
  "description": "",
  "keyword": "Feature"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "acesso a tela \"https://qa-test.avenuecode.com\" acesso o link informo email de usuario \"paulovlrs@hotmail.com.br\" e senha \"123456789\"",
  "keyword": "Given "
});
formatter.match({
  "location": "BDDTask.acessoATelaAcessoOLinkInformoEmailDeUsuarioESenha(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "acionei o comando My Tasks",
  "keyword": "And "
});
formatter.match({
  "location": "BDDTask.acioneiOComandoMyTasks()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Criar uma Task salvando com Enter",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "preencho o campo de descricao da nova task \"prova1\"",
  "keyword": "When "
});
formatter.match({
  "location": "BDDTask.preenchoOCampoDeDescricaoDaNovaTask(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "aciono comando adicionar usando as teclas do teclado",
  "keyword": "And "
});
formatter.match({
  "location": "BDDTask.acionoComandoAdicionarUsandoAsTeclasDoTeclado()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "uma nova task criada",
  "keyword": "Then "
});
formatter.match({
  "location": "BDDTask.umaNovaTaskCriada()"
});
formatter.result({
  "status": "skipped"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "acesso a tela \"https://qa-test.avenuecode.com\" acesso o link informo email de usuario \"paulovlrs@hotmail.com.br\" e senha \"123456789\"",
  "keyword": "Given "
});
formatter.match({
  "location": "BDDTask.acessoATelaAcessoOLinkInformoEmailDeUsuarioESenha(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "acionei o comando My Tasks",
  "keyword": "And "
});
formatter.match({
  "location": "BDDTask.acioneiOComandoMyTasks()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Criar uma Task salvando usando o mouse",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "preencho o campo de descricao da nova task \"prova2\"",
  "keyword": "When "
});
formatter.match({
  "location": "BDDTask.preenchoOCampoDeDescricaoDaNovaTask(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "aciono comando adicionar com o mouse",
  "keyword": "And "
});
formatter.match({
  "location": "BDDTask.acionoComandoAdicionarComOMouse()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "uma nova task criada",
  "keyword": "Then "
});
formatter.match({
  "location": "BDDTask.umaNovaTaskCriada()"
});
formatter.result({
  "status": "skipped"
});
});